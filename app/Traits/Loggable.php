<?php

namespace App\Traits;

use Auth;
use App\History;

trait Loggable
{
    public static function bootLoggable()
    {
        static::saved(function ($model) {
            $dirty = $model->getDirty();
            if (array_key_exists('assignee_id', $dirty)) {
                History::create([
                    'loggable_type' => $model->getTable(),
                    'loggable_id'=> $model->id,
                    'action' => 'assign',
                    'value' => $dirty['assignee_id'],
                ]);    
            }
        });

    }
}