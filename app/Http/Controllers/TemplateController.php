<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\QueryBuilder\QueryBuilder;
use App\Template;
use App\Checklist;
use App\Item;
use Carbon\Carbon;

class TemplateController extends Controller
{
    protected function getParams()
    {
        return $this->attributes()->only([
            'name',
            'checklist',
            'items',    
        ])->toArray();
    }    

    public function index()
    {
        $query = QueryBuilder::for(Template::class)
            ->allowedFilters('name');
        return response()->json($this->transform($query));
    }

    public function store()
    {
        $input = $this->getParams();
        $template = new Template;
        $template->fill($input);
        $template->save();        
        return response()->json($this->transform($template));
    }

    public function show($id)
    {
        $template = Template::find($id);
        if (empty($template)) {
            return $this->notFound();
        }
        return response()->json($this->transform($template));
    }

    public function update($id)
    {
        $template = Template::find($id);
        if (empty($template)) {
            return $this->notFound();
        }
        $input = $this->getParams();
        $template->fill($input);
        $template->save();
        return response()->json($this->transform($template));
    }
    
    public function destroy($id)
    {
        $template = Template::find($id);
        if (empty($template)) {
            return $this->notFound();
        }
        $template->delete();
        return response()->json(null, 204);
    }

    public function assigns($id)
    {
        $template = Template::find($id);
        if (empty($template)) {
            return $this->notFound();
        }
        $data = request()->input('data');
        $results = collect();
        \Log::debug('data', $data);
        \DB::transaction(function () use ($data, $template, &$results) {
            foreach ($data as $d) {            
                $now = Carbon::now();                
                $checklist = Checklist::create([
                    'object_domain' => $d['attributes']['object_domain'],
                    'object_id' => $d['attributes']['object_id'],
                    'description' => $template->checklist['description'],
                    'due' => $now->copy()->add($template->checklist['due_unit'], $template->checklist['due_interval']),
                ]);
                
                if (count($template->items) > 0) {
                    $items = array_map(function ($t) use ($now) {
                        return Item::make([
                            'description' => $t['description'],
                            'urgency' => $t['urgency'],
                            'due' => $now->copy()->add($t['due_unit'], $t['due_interval']),
                        ])->toArray();
                    }, $template->items);
                    $checklist->items()->createMany($items);
                }

                $checklist->load('items');
                $results->push($checklist);            
            }
        });
        return response()->json($this->transform($results));
    }
}
