<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Spatie\QueryBuilder\QueryBuilder as QueryBuilder;  
use Illuminate\Database\Query\Builder as Builder;
use Illuminate\Database\Eloquent\Model as Model;
use Illuminate\Support\Collection as Collection;

class Controller extends BaseController
{
    protected function notFound($msg = 'Data not found') {
        return response()->json(['error' => $msg], 404);
    }

    protected function attributes(){
        $attributes = request()->input('data');
        return collect(isset($attributes['attributes']) ? $attributes['attributes'] : array_get($attributes, 'attribute', []));
    }

    protected function transform($model)
    {        
        // Builder
        if ($model instanceof QueryBuilder || $model instanceof Builder) {
            $limit = request()->input('page.limit', 10);
            $offset = request()->input('page.offset', 0);    
            $total = $model->count();
            $max_offset = (ceil($total / $limit) - 1) * $limit;
            $prev_url = $offset - $limit >= 0 ? request()->fullUrlWithQuery(['page' => ['limit' => $limit, 'offset' => $offset - $limit]]) : null;
            $next_url = $offset + $limit <= $total ? request()->fullUrlWithQuery(['page' => ['limit' => $limit, 'offset' => $offset + $limit]]) : null;
            $first_url = request()->fullUrlWithQuery(['page' => ['limit' => $limit, 'offset' => 0]]);
            $last_url = request()->fullUrlWithQuery(['page' => ['limit' => $limit, 'offset' => $max_offset]]);
            $items = $model->limit($limit)->offset($offset)->get();
            $data = [];            
            foreach ($items as $item) {
                $data[] = $this->transform($item)['data'];
            }            
            return [
                'data' => $data,
                'meta' => [
                    'total' => $total,
                    'count' => $items->count(),
                ],
                'links' => [
                    'first' => $first_url,
                    'previous' => $prev_url,
                    'next' => $next_url,
                    'last' => $last_url,
                ]
            ];

        } 
        
        // Collection
        else if ($model instanceof Collection) {
            $data = [];            
            foreach ($model as $m) {
                $data[] = $this->transform($m)['data'];
            }
            return [
                'data' => $data,
                'meta' => [
                    'total' => $model->count(),
                ],
            ];
        } 
        
        // Model
        else if ($model instanceof Model) {
            $model->refresh();
            $table_name = $model->getTable();
            $attributes = collect($model->toArray())->except('id');
            return [
                'data' => [
                    'type' => $table_name,
                    'id' => $model->id,
                    'attributes' => $attributes,
                    'links' => [
                        'self' => route($table_name.'.show', ['id' => $model->id])
                    ],
                ]
            ];    
        }

        // unknown type
        return $model;
    }
}
