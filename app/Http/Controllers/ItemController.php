<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\QueryBuilder\QueryBuilder;
use App\Item;
use Carbon\Carbon;

class ItemController extends Controller
{
    protected function transform($model)
    {
        $attributes = collect($model->toArray())->except('id');
        $table_name = $model->checklist->getTable();
        return [
            'data' => [
                'type' => $table_name,
                'id' => $model->id,
                'attributes' => $attributes,
                'links' => [
                    'self' => route($table_name.'.show', ['id' => $model->checklist->id])
                ],
            ]
        ]; 
    }

    private function updateCompletedAt($now = null)
    {
        $results = collect();
        $data = request()->input('data', []);
        if (count($data) > 0) {
            $item_ids = array_map(function ($d) { return $d['item_id']; }, $data);
            $items = Item::whereIn('id', $item_ids)->get();
            \DB::transaction(function () use (&$results, $items, $now) {
                foreach ($items as $r) {
                    $r->completed_at = $now;
                    $r->save();
                    $o = $r->only(['id', 'is_completed', 'checklist_id']);
                    $o['item_id'] = $r->id;
                    $results->push($o);
                }        
            });
        }
        return response()->json(['data' => $results]);
    }

    public function complete()
    {
        $now = Carbon::now();        
        return $this->updateCompletedAt($now);
    }

    public function incomplete()
    {
        return $this->updateCompletedAt();
    }

    public function store($id)
    {
        $data = collect(request()->only('data')['data']['attribute']);
        $input = $data->only([
            'description',
            'completed_at',
            'due',
            'urgency',
            'assignee_id',
            'task_id',
        ])->all();
        $model = new Item;
        $model->fill($input);
        $model->checklist_id = $id;
        $model->save();
        $model->refresh();
        return response()->json($this->transform($model)); 
    }

    public function show($id, $itemId)
    {
        $model = Item::where('checklist_id', $id)->where('id', $itemId)->first();
        if (empty($model)) {
            return $this->notFound();
        }
        return response()->json($this->transform($model)); 
    }

    public function update($id, $itemId)
    {
        $model = Item::where('checklist_id', $id)->where('id', $itemId)->first();
        if (empty($model)) {
            return $this->notFound();
        }
        $data = collect(request()->only('data')['data']['attribute']);
        $input = $data->only([
            'description',
            'completed_at',
            'due',
            'urgency',
            'assignee_id',
            'task_id',
        ])->all();
        $model->fill($input);
        $model->checklist_id = $id;
        $model->save();
        return response()->json($this->transform($model)); 
    }

    public function destroy($id, $itemId){
        $model = Item::where('checklist_id', $id)->where('id', $itemId)->first();
        if (empty($model)) {
            return $this->notFound();
        }
        $model->delete();
        return response()->json(null, 204);
    }

    public function bulkUpdate($id)
    {
        $results = [];
        $data = request()->only('data')['data'];        
        foreach ($data as $d) {
            if ($d['action'] == 'update') {    
                $item = Item::find($d['id']);
                if (empty($item)) {
                    $status = 404;
                } else if ($item->checklist_id != $id) {
                    $status = 403;
                } else {
                    try {
                        $input = collect($d['attributes'])->only([
                            'description',
                            'completed_at',
                            'due',
                            'urgency',
                            'assignee_id',
                            'task_id',
                        ])->all();
                        $item->fill($input);
                        $item->save();
                        $status = 200;
                    } catch (\Exception $e) {
                        $status = 500;
                    }
                }
                $results[] = [
                    'id' => $d['id'],
                    'action' => $d['action'],
                    'status' => $status,
                ];
            } else if ($d['action'] == 'create') {    
                $item = new Item;
                if (empty($item)) {
                    $status = 404;
                } else if ($item->checklist_id != $id) {
                    $status = 403;
                } else {
                    try {
                        $input = collect($d['attributes'])->only([
                            'description',
                            'completed_at',
                            'due',
                            'urgency',
                            'assignee_id',
                            'task_id',
                        ])->all();
                        $item->fill($input);
                        $item->save();
                        $status = 200;
                    } catch (\Exception $e) {
                        $status = 500;
                    }
                }
                $results[] = [
                    'id' => $item->id,
                    'action' => $d['action'],
                    'status' => $status,
                ];
            } else if ($d['action'] == 'delete') {
                $item = Item::find($d['id']);
                if (empty($item)) {
                    $status = 404;
                } else if ($item->checklist_id != $id) {
                    $status = 403;
                } else {
                    try {                        
                        $item->delete();
                        $status = 200;
                    } catch (\Exception $e) {
                        $status = 500;
                    }
                }
                $results[] = [
                    'id' => $d['id'],
                    'action' => $d['action'],
                    'status' => $status,
                ];
            }
        }
        return response()->json(['data' => $results]);
    }

    public function summaries()
    {
        $this->validate(request(), [
            'date' => 'required|date',
        ]);
        $date = request()->input('date', Carbon::now()->toDateTimeString());
        $object_domain = request()->input('object_domain');
        $tz = request()->input('tz', 'UTC');
        $date = Carbon::parse($date, $tz);
        $today = Item::ofDomain($object_domain)
            ->whereNull('items.completed_at')
            ->whereDate('items.due', '=', $date->toDateString())
            ->count();
        $past_due = Item::ofDomain($object_domain)
            ->whereNull('items.completed_at')
            ->where('items.due', '<', $date->toDateTimeString())
            ->count();
        $this_week = Item::ofDomain($object_domain)
            ->whereNull('items.completed_at')
            ->whereDate('items.due', '>=', $date->startOfWeek()->toDateString())
            ->whereDate('items.due', '<=', $date->endOfWeek()->toDateString())
            ->count();
        $date_last_week = $date->copy()->sub('week', 1);
        $past_week = Item::ofDomain($object_domain)
            ->whereNull('items.completed_at')
            ->whereDate('items.due', '>=', $date_last_week->startOfWeek()->toDateString())
            ->whereDate('items.due', '<=', $date_last_week->endOfWeek()->toDateString())
            ->count();
        $this_month = Item::ofDomain($object_domain)
            ->whereNull('items.completed_at')
            ->whereDate('items.due', '>=', $date->startOfMonth()->toDateString())
            ->whereDate('items.due', '<=', $date->endOfMonth()->toDateString())
            ->count();
        $date_last_month = $date->copy()->sub('month', 1);
        $past_month = Item::ofDomain($object_domain)
            ->whereNull('items.completed_at')
            ->whereDate('items.due', '>=', $date_last_month->startOfMonth()->toDateString())
            ->whereDate('items.due', '<=', $date_last_month->endOfMonth()->toDateString())
            ->count();
        $total = Item::ofDomain($object_domain)
            ->whereNull('items.completed_at')
            ->count();
        return response()->json([
            'data' => [
                'today' => $today,
                'past_due' => $past_due,
                'this_week' => $this_week,
                'past_week' => $past_week,
                'this_month' => $this_month,
                'past_month' => $past_month,
                'total' => $total,
            ]
        ]);
    }
}
