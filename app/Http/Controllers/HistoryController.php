<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\QueryBuilder\QueryBuilder;
use App\History;

class HistoryController extends Controller
{
    protected function getParams()
    {
        return $this->attributes()->only([
            'loggable_type',
            'loggable_id',
            'action',
            'kwuid',
            'value',    
        ])->toArray();
    }    

    public function index()
    {
        $query = QueryBuilder::for(History::class)
            ->allowedFilters('loggable_type', 'loggable_id', 'action', 'kwuid', 'value');
        return response()->json($this->transform($query));
    }

    public function show($id)
    {
        $history = History::find($id);
        if (empty($history)) {
            return $this->notFound();
        }
        return response()->json($this->transform($history));
    }
}
