<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\QueryBuilder\QueryBuilder;
use App\Checklist;
use App\Item;

class ChecklistController extends Controller
{
    protected function getParams()
    {
        return $this->attributes()->only([
            'object_domain',
            'object_id',
            'due',
            'urgency',
            'description',
            'completed_at',
            'items',
            'task_id',            
        ])->toArray();
    }    

    public function index()
    {
        $query = QueryBuilder::for(Checklist::class)
            ->allowedFilters('object_domain', 'object_id', 'description');
        return response()->json($this->transform($query));
    }

    public function store()
    {
        $input = $this->getParams();
        $checklist = new Checklist;
        \DB::transaction(function () use (&$checklist, $input) {
            $checklist->fill($input);
            $checklist->save();
            if (array_key_exists('items', $input) && count($input['items']) > 0) {
                $items = array_map(function ($desc) use ($input) {
                    $item = new Item;
                    $item->fill($input);
                    $item->description = $desc;
                    return $item->toArray();
                }, $input['items']);
                $checklist->items()->createMany($items);
            }
        });
        return response()->json($this->transform($checklist));
    }

    public function show($id)
    {
        $checklist = Checklist::find($id);
        if (empty($checklist)) {
            return $this->notFound();
        }
        return response()->json($this->transform($checklist));
    }

    public function showItems($id)
    {
        $checklist = Checklist::with('items')->find($id);
        if (empty($checklist)) {
            return $this->notFound();
        }
        return response()->json($this->transform($checklist));
    }


    public function update($id)
    {
        $checklist = Checklist::find($id);
        if (empty($checklist)) {
            return $this->notFound();
        }
        $input = $this->getParams();
        $checklist->fill($input);
        $checklist->save();
        return response()->json($this->transform($checklist));
    }
    
    public function destroy($id)
    {
        $checklist = Checklist::find($id);
        if (empty($checklist)) {
            return $this->notFound();
        }
        $checklist->delete();
        return response()->json(null, 204);
    }
}
