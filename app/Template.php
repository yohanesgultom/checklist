<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;

class Template extends Model
{
    const DUE_UNITS = [
        'minute',
        'hour',
        'day',
    ];

    protected $fillable = [
        'name',
        'checklist',
        'items',
    ];

    protected $casts = [
        'checklist' => 'array',
        'items' => 'array'
    ];

}