<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Checklist extends Model
{
    protected $fillable = [
        'object_domain',
        'object_id',
        'description',
        'due',
        'urgency',
        'completed_at',
        'created_by',
        'updated_by',
    ];

    protected $dates = [
        'completed_at',
        'due',
    ];

    protected $appends = [
        'is_completed',
    ];

    public function getIsCompletedAttribute()
    {
        return !empty($this->completed_at);
    }

    public function items()
    {
        return $this->hasMany('App\Item');
    }
}