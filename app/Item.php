<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Loggable;

class Item extends Model
{
    use Loggable;

    protected $fillable = [
        'description',
        'completed_at',
        'due',
        'urgency',
        'updated_by',
        'assignee_id',
        'checklist_id',
        'task_id',
    ];

    protected $dates = [
        'completed_at',
        'due',
    ];

    protected $appends = [
        'is_completed',
    ];

    public function getIsCompletedAttribute()
    {
        return !empty($this->completed_at);
    }
    
    public function checklist()
    {
        return $this->belongsTo('App\Checklist');
    }

    public function scopeOfDomain($query, $object_domain)
    {
        $query = $query->join('checklists', 'checklists.id', 'items.checklist_id');
        if (!empty($object_domain)) {
            $query = $query->where('checklists.object_domain', $object_domain);
        }
        return $query;
    }
}