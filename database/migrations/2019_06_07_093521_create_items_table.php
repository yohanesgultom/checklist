<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('description')->nullable();
            $table->dateTime('completed_at')->nullable();
            $table->dateTime('due')->nullable();
            $table->string('urgency')->default(0);
            $table->string('updated_by')->nullable();
            $table->string('assignee_id')->nullable();
            $table->unsignedBigInteger('checklist_id')->nullable();
            $table->unsignedInteger('task_id')->nullable();
            $table->foreign('checklist_id')->references('id')->on('checklists');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
