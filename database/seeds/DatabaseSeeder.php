<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Faker\Factory as Faker;
use Carbon\Carbon;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $now = Carbon::now();

        // User
        $user = new App\User;
        $user->name = $faker->name();
        $user->email = $faker->email;
        $user->password = Hash::make('password');
        $user->api_token = 'thisisanapitoken';
        $user->save();

        // Checklist
        for ($i = 0; $i < 20; $i++) {
            $due = $now->copy()->add('days', $i + $faker->numberBetween(7, 30));
            $c = App\Checklist::create([
                'object_domain' => 'Domain '.$faker->numberBetween(1, 5),
                'object_id' => str_pad("$i", 5, '0', STR_PAD_LEFT),
                'description' => 'Checklist '.$faker->sentence(),
                'urgency' => $faker->numberBetween(0, 3),
                'due' => $due,
            ]);

            for ($j = 0; $j < 3; $j++) {
                App\Item::create([
                    'checklist_id' => $c->id,
                    'description' => 'Item '.$faker->sentence(),
                    'assignee_id' => str_pad("$j", 5, '0', STR_PAD_LEFT),
                    'task_id' => $faker->numberBetween(1, 100),
                    'urgency' => $faker->numberBetween(0, 3),
                    'due' => $due,
                ]);
            }
        }

        // Template
        for ($i = 0; $i < 10; $i++) {
            $items = [];
            for ($j = 0; $j < 3; $j++) {
                $items[] = [
                    'description' => 'Item '.$faker->sentence(),
                    'urgency' => $faker->numberBetween(0, 3),
                    'due_interval' => $faker->numberBetween(1, 10),
                    'due_unit' => $faker->randomElement(App\Template::DUE_UNITS),
                ];
            }
            App\Template::create([
                'name' => $faker->sentence(),
                'checklist' => [
                    'description' => "Template {$i}",
                    'due_interval' => $faker->numberBetween(1, 10),
                    'due_unit' => $faker->randomElement(App\Template::DUE_UNITS),
                ],
                'items' => $items,
            ]);
        }
    }
}
