<?php
use Carbon\Carbon;
use App\Checklist;
use App\Item;

class ItemTest extends TestCase
{
    public function testComplete()
    {
        $items = Item::inRandomOrder()->whereNull('completed_at')->select('id')->limit(3)->get();
        $params = [
            'data' => $items->map(function ($o) { return ['item_id' => $o->id ]; })->all(),
        ];
        $response = $this->actingAs($this->user)->call('POST', 'checklists/complete', $params);
        $this->assertStatus(200, $response);
        $res = json_decode($this->response->getContent());
        // print(json_encode($res, JSON_PRETTY_PRINT));
        $this->assertEquals($items->count(), count($res->data));
        $actual = Item::whereIn('id', $items->pluck('id')->all());
        foreach ($actual as $item) {
            $this->assertNotNull($item->completed_at);
            $this->assertTrue($item->is_completed);
        }
    }

    public function testIncomplete()
    {
        Item::inRandomOrder()->whereNull('completed_at')->update(['completed_at' => Carbon::now()]);
        $items = Item::inRandomOrder()->whereNotNull('completed_at')->select('id')->limit(3)->get();
        $params = [
            'data' => $items->map(function ($o) { return ['item_id' => $o->id ]; })->all(),
        ];
        $response = $this->actingAs($this->user)->call('POST', 'checklists/incomplete', $params);
        $this->assertStatus(200, $response);
        $res = json_decode($this->response->getContent());        
        $this->assertEquals($items->count(), count($res->data));
        $actual = Item::whereIn('id', $items->pluck('id')->all());
        foreach ($actual as $item) {
            $this->assertNull($item->completed_at);
            $this->assertFalse($item->is_completed);
        }
    }

    public function testStore()
    {
        $checklist = Checklist::inRandomOrder()->first();
        $params = [
            'data' => [
                'attribute' => [
                    'description' => 'Item '.$this->faker->sentence(),
                    'urgency' => $this->faker->numberBetween(0, 3),
                    'due' => $checklist->due,
                    'task_id' => $this->faker->numberBetween(1, 100),
                ]
            ]
        ];
        $response = $this->actingAs($this->user)->call('POST', "checklists/{$checklist->id}/items", $params);
        $this->assertStatus(200, $response);
        $res = json_decode($this->response->getContent());
        // print(json_encode($res, JSON_PRETTY_PRINT));
        $expected = $params['data']['attribute'];
        $actual = Item::find($res->data->id);
        $this->assertNotNull($actual);
        $this->assertEquals($expected['description'], $actual->description);
        $this->assertEquals($expected['urgency'], $actual->urgency);
        $this->assertEquals($expected['due'], $actual->due->toDateTimeString());
        $this->assertFalse($actual->is_completed);
    }

    public function testShow()
    {
        $expected = Item::inRandomOrder()->first();
        $response = $this->actingAs($this->user)->call('GET', "checklists/{$expected->checklist_id}/items/{$expected->id}");
        $this->assertStatus(200, $response);
        $res = json_decode($this->response->getContent());
        // print(json_encode($res, JSON_PRETTY_PRINT));
        $actual = $res->data->attributes;
        $this->assertEquals($expected->description, $actual->description);
        $this->assertEquals($expected->urgency, $actual->urgency);
        $this->assertEquals($expected->due, $actual->due);
        $this->assertEquals($expected->assignee_id, $actual->assignee_id);
        $this->assertEquals($expected->completed_at, $actual->completed_at);
    }

    public function testUpdate()
    {
        $item = Item::inRandomOrder()->first();
        $due = $item->due->sub('days', 1);
        $params = [
            'data' => [
                'attribute' => [
                    'description' => 'Item '.$this->faker->sentence(),
                    'urgency' => $this->faker->numberBetween(0, 3),
                    'due' => $due,
                    'task_id' => $this->faker->numberBetween(1, 100),
                ]
            ]
        ];
        $response = $this->actingAs($this->user)->call('PATCH', "checklists/{$item->checklist_id}/items/{$item->id}", $params);
        $this->assertStatus(200, $response);
        $res = json_decode($this->response->getContent());
        // print(json_encode($res, JSON_PRETTY_PRINT));
        $expected = $params['data']['attribute'];
        $actual = Item::find($item->id);
        $this->assertNotNull($actual);
        $this->assertEquals($expected['description'], $actual->description);
        $this->assertEquals($expected['urgency'], $actual->urgency);
        $this->assertEquals($expected['due'], $actual->due->toDateTimeString());
        $this->assertFalse($actual->is_completed);
    }

    public function testDestroy()
    {
        $existing = Item::inRandomOrder()->first();
        $response = $this->actingAs($this->user)->call('DELETE', "checklists/{$existing->checklist_id}/items/{$existing->id}");
        $this->assertStatus(204, $response);
        $actual = Item::find($existing->id);
        $this->assertNull($actual);
    }

    public function testBulkUpdate()
    {
        $checklist = Checklist::with('items')->inRandomOrder()->first();
        $data = $checklist->items->map(function ($item) {
            return [
                'id' => $item->id,
                'action' => 'update',
                'attributes' => [
                    'description' => 'Item '.$this->faker->sentence(),
                    'urgency' => $this->faker->numberBetween(0, 3),
                    'due' => $item->due->sub('days', 1),
                ]
            ];
        })->all();
        $params = ['data' => $data];
        $response = $this->actingAs($this->user)->call('POST', "checklists/{$checklist->id}/items/_bulk", $params);
        $this->assertStatus(200, $response);
        $res = json_decode($this->response->getContent(), true);
        // print(json_encode($res, JSON_PRETTY_PRINT));
        $expected = array_map(function ($d) {
            return [
                'id' => $d['id'],
                'action' => $d['action'],
                'status' => 200,
            ];
        }, $data);
        $this->assertEquals($expected, $res['data']);
        // check database
        $updated = Checklist::with('items')->find($checklist->id);
        $actual = $updated->items->map(function ($item) {
            return [
                'id' => $item->id,
                'action' => 'update',
                'attributes' => [
                    'description' => $item->description,
                    'urgency' => $item->urgency,
                    'due' => $item->due,
                ]
            ];
        })->all();
        $this->assertEquals($data, $actual);
    }

    public function testSummaries()
    {
        $checklist = Checklist::inRandomOrder()->first();
        $params = [
            'date' => $checklist->due->toIso8601String(),
            'object_domain' => $checklist->object_domain,
        ];
        $response = $this->actingAs($this->user)->call('GET', "checklists/items/summaries", $params);
        $this->assertStatus(200, $response);
        $res = json_decode($this->response->getContent());
        // print(json_encode($res, JSON_PRETTY_PRINT));
        $this->assertNotNull($res->data->today);
        $this->assertNotNull($res->data->past_due);
        $this->assertNotNull($res->data->this_week);
        $this->assertNotNull($res->data->past_week);
        $this->assertNotNull($res->data->this_month);
        $this->assertNotNull($res->data->past_month);
        $this->assertNotNull($res->data->total);
    }
}
