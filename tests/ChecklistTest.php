<?php
use App\Checklist;
use Carbon\Carbon;

class ChecklistTest extends TestCase
{
    public function testIndex()
    {
        # pagination
        $response = $this->actingAs($this->user)->call('GET', 'checklists');
        $this->assertStatus(200, $response);
        $actual = $response->getData();
        $expected = Checklist::count();
        $limit = 10;
        $max_offset = (ceil($expected / $limit) - 1) * $limit;
        $this->assertEquals($expected, $actual->meta->total);
        $this->assertQueryParams($actual->links->first, ['page' => ['limit' => $limit, 'offset' => 0]]);
        $this->assertQueryParams($actual->links->last, ['page' => ['limit' => $limit, 'offset' => $max_offset]]);
        
        # filter
        $params = ['filter' => ['description' => ['like' => 'vel']]];
        $response = $this->actingAs($this->user)->call('GET', 'checklists', $params);
        $this->assertStatus(200, $response);
        $actual = $response->getData();
        $this->assertNotNull($actual->data);
        $expected = Checklist::where('description', 'like', '%'.$params['filter']['description']['like'].'%')->count();
        $this->assertEquals($expected, $actual->meta->total);
        $this->assertQueryParams($actual->links->first, array_merge($params, ['page' => ['limit' => $limit, 'offset' => 0]]));        
        // print(json_encode($actual, JSON_PRETTY_PRINT));
    }

    public function testStore()
    {
        $due = Carbon::now()->add('days', $this->faker->numberBetween(7, 30));
        $params = [
            'data' => [
                'attributes' => [
                    'object_domain' => 'Domain '.$this->faker->word,
                    'object_id' => $this->faker->numerify('######'),
                    'description' => 'Checklist '.$this->faker->sentence(),
                    'urgency' => $this->faker->numberBetween(0, 3),
                    'due' => $due,
                    'task_id' => $this->faker->numberBetween(1, 100),
                    'items' => $this->faker->sentences(),
                ]
            ]
        ];
        $response = $this->actingAs($this->user)->call('POST', 'checklists', $params);
        $this->assertStatus(200, $response);
        $res = json_decode($this->response->getContent());
        $expected = $params['data']['attributes'];
        $actual = Checklist::with('items')->find($res->data->id);
        $this->assertNotNull($actual);
        $this->assertEquals($expected['object_domain'], $actual->object_domain);
        $this->assertEquals($expected['object_id'], $actual->object_id);
        $this->assertEquals($expected['description'], $actual->description);
        $this->assertEquals($expected['urgency'], $actual->urgency);
        $this->assertEquals($expected['due'], $actual->due->toDateTimeString());
        $this->assertEquals($expected['items'], $actual->items->pluck('description')->all());
    }

    public function testShow()
    {
        $existing = Checklist::inRandomOrder()->first();
        $response = $this->actingAs($this->user)->call('GET', "checklists/{$existing->id}");
        $actual = $response->getData();
        $this->assertNotNull($actual->data);
        $data = $actual->data; 
        $this->assertEquals($existing->id, $data->id);
        $this->assertEquals($existing->object_domain, $data->attributes->object_domain);
        $this->assertEquals($existing->object_id, $data->attributes->object_id);
        $this->assertEquals($existing->description, $data->attributes->description);
        $this->assertEquals($existing->urgency, $data->attributes->urgency);
        $this->assertEquals($existing->due, $data->attributes->due);
    }

    public function testUpdate()
    {
        $existing = Checklist::inRandomOrder()->first();
        $params = [
            'data' => [
                'attributes' => [
                    'description' => 'Checklist '.$this->faker->sentence(),
                    'completed_at' => Carbon::now(),
                ]
            ]
        ];
        $response = $this->actingAs($this->user)->call('PATCH', "checklists/{$existing->id}", $params);
        $this->assertStatus(200, $response);
        $res = json_decode($this->response->getContent());
        $expected = $params['data']['attributes'];
        $actual = Checklist::find($existing->id);
        $this->assertNotNull($actual);
        $this->assertEquals($expected['description'], $actual->description);
        $this->assertEquals($expected['completed_at'], $actual->completed_at->toDateTimeString());
    }

    public function testDestroy()
    {
        $existing = Checklist::inRandomOrder()->first();
        $response = $this->actingAs($this->user)->call('DELETE', "checklists/{$existing->id}");
        $this->assertStatus(204, $response);
        $actual = Checklist::find($existing->id);
        $this->assertNull($actual);
    }

    public function testShowItems()
    {
        $existing = Checklist::with('items')->inRandomOrder()->first();
        $response = $this->actingAs($this->user)->call('GET', "checklists/{$existing->id}/items");
        $actual = $response->getData();
        // print(json_encode($actual, JSON_PRETTY_PRINT));
        $this->assertNotNull($actual->data);
        $data = $actual->data; 
        $this->assertNotNull($data->attributes->items);
        $this->assertEquals(count($existing->items), count($data->attributes->items));
        $this->assertEquals($existing->id, $data->id);
        $this->assertEquals($existing->object_domain, $data->attributes->object_domain);
        $this->assertEquals($existing->object_id, $data->attributes->object_id);
        $this->assertEquals($existing->description, $data->attributes->description);
        $this->assertEquals($existing->urgency, $data->attributes->urgency);
        $this->assertEquals($existing->due, $data->attributes->due);
    }

}
