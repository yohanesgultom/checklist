<?php
use Laravel\Lumen\Testing\DatabaseMigrations;
use Illuminate\Support\Facades\Artisan;
use Faker\Factory as Faker;

abstract class TestCase extends Laravel\Lumen\Testing\TestCase
{
    use DatabaseMigrations;

    public $faker;
    public $user;

    /**
     * Creates the application.
     *
     * @return \Laravel\Lumen\Application
     */
    public function createApplication()
    {
        return require __DIR__.'/../bootstrap/app.php';
    }

    protected function setUp(): void
    {
        parent::setUp();
        Artisan::call('db:seed');
        $this->faker = Faker::create();
        $this->baseUrl = env('APP_URL'); 
        $this->user = \App\User::first();
    }

    public function assertStatus($status, $response)
    {
        $res_status = $response->status();
        if ($res_status != $status) {
            print($response->content());
        }
        $this->assertEquals($status, $res_status);
    }

    public function assertQueryParams($url, $expected)
    {
        parse_str(parse_url($url)['query'], $actual);
        $this->assertEquals($expected, $actual);
    }

}
