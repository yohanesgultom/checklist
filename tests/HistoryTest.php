<?php
use App\History;
use Carbon\Carbon;

class HistoryTest extends TestCase
{    
    public function testIndex()
    {
        $response = $this->actingAs($this->user)->call('GET', 'histories');
        $this->assertStatus(200, $response);
        $actual = $response->getData();
        // print(json_encode($actual, JSON_PRETTY_PRINT));
        $this->assertNotNull($actual->data);
        $expected = History::count();
        $this->assertEquals($expected, $actual->meta->total);
    }

    public function testShow()
    {
        $existing = History::inRandomOrder()->first();
        $response = $this->actingAs($this->user)->call('GET', "histories/{$existing->id}");
        $actual = $response->getData();
        $this->assertNotNull($actual->data);
        $data = $actual->data; 
        $this->assertEquals($existing->id, $data->id);
        $this->assertEquals($existing->loggable_type, $data->attributes->loggable_type);
        $this->assertEquals($existing->loggable_id, $data->attributes->loggable_id);
        $this->assertEquals($existing->kwuid, $data->attributes->kwuid);
        $this->assertEquals($existing->value, $data->attributes->value);
    }

}
