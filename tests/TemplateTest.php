<?php
use App\Template;
use App\Checklist;
use Carbon\Carbon;

class TemplateTest extends TestCase
{
    public function testIndex()
    {
        $response = $this->actingAs($this->user)->call('GET', 'templates');
        $this->assertStatus(200, $response);
        $actual = $response->getData();
        // print(json_encode($actual, JSON_PRETTY_PRINT));
        $this->assertNotNull($actual->data);
        $expected = Template::count();
        $this->assertEquals($expected, $actual->meta->total);
    }

    public function testStore()
    {
        $items = [];
        for ($j = 0; $j < 3; $j++) {
            $items[] = [
                'description' => 'Item '.$this->faker->sentence(),
                'urgency' => $this->faker->numberBetween(0, 3),
                'due_interval' => $this->faker->numberBetween(1, 10),
                'due_unit' => $this->faker->randomElement(Template::DUE_UNITS),
            ];
        }
        $params = [
            'data' => [
                'attributes' => [
                    'name' => $this->faker->sentence(),
                    'checklist' => [
                        'description' => "Template Test",
                        'due_interval' => $this->faker->numberBetween(1, 10),
                        'due_unit' => $this->faker->randomElement(Template::DUE_UNITS),
                    ],
                    'items' => $items,
                ]
            ]
        ];
        $response = $this->actingAs($this->user)->call('POST', 'templates', $params);
        $this->assertStatus(200, $response);
        $res = json_decode($this->response->getContent());
        // print(json_encode($res, JSON_PRETTY_PRINT));
        $expected = $params['data']['attributes'];
        $actual = Template::find($res->data->id);
        $this->assertNotNull($actual);
        $this->assertEquals($expected['name'], $actual->name);
        $this->assertEquals($expected['checklist'], $actual->checklist);
        $this->assertEquals($expected['items'], $actual->items);
    }

    public function testShow()
    {
        $existing = Template::inRandomOrder()->first();
        $response = $this->actingAs($this->user)->call('GET', "templates/{$existing->id}");
        $res = json_decode($this->response->getContent(), true);
        // print_r($res);
        $data = $res['data']['attributes']; 
        $this->assertEquals($existing->id, $res['data']['id']);
        $this->assertEquals($existing->name, $data['name']);
        $this->assertEquals($existing->checklist, $data['checklist']);
        $this->assertEquals($existing->items, $data['items']);
    }
    
    public function testUpdate()
    {
        $existing = Template::inRandomOrder()->first();
        $items = [];
        for ($j = 0; $j < 3; $j++) {
            $items[] = [
                'description' => 'Item '.$this->faker->sentence(),
                'urgency' => $this->faker->numberBetween(0, 3),
                'due_interval' => $this->faker->numberBetween(1, 10),
                'due_unit' => $this->faker->randomElement(Template::DUE_UNITS),
            ];
        }
        $params = [
            'data' => [
                'id' => $existing->id,
                'attributes' => [
                    'name' => $this->faker->sentence(),
                    'checklist' => [
                        'description' => "Template Test Update",
                        'due_interval' => $this->faker->numberBetween(1, 10),
                        'due_unit' => $this->faker->randomElement(Template::DUE_UNITS),
                    ],
                    'items' => $items,
                ]
            ]
        ];
        $response = $this->actingAs($this->user)->call('PATCH', "templates/{$existing->id}", $params);
        $this->assertStatus(200, $response);
        $res = json_decode($this->response->getContent());
        $expected = $params['data']['attributes'];
        $actual = Template::find($existing->id);
        $this->assertNotNull($actual);
        $this->assertEquals($expected['name'], $actual->name);
        $this->assertEquals($expected['checklist'], $actual->checklist);
        $this->assertEquals($expected['items'], $actual->items);
    }

    public function testDestroy()
    {
        $existing = Template::inRandomOrder()->first();
        $response = $this->actingAs($this->user)->call('DELETE', "templates/{$existing->id}");
        $this->assertStatus(204, $response);
        $actual = Template::find($existing->id);
        $this->assertNull($actual);
    }

    public function testAssigns()
    {
        $object_domain = 'Test Domain';
        $params = [
            'data' => [
                ['attributes' => ['object_id' => 1, 'object_domain' => $object_domain]],
                ['attributes' => ['object_id' => 2, 'object_domain' => $object_domain]],
            ]
        ];
        $existing = Template::inRandomOrder()->first();
        $response = $this->actingAs($this->user)->call('POST', "templates/{$existing->id}/assigns", $params);
        $this->assertStatus(200, $response);
        $res = json_decode($this->response->getContent());
        // print(json_encode($res, JSON_PRETTY_PRINT));
        $checklists = Checklist::where('object_domain', $object_domain)->get();
        $this->assertEquals(count($params['data']), $checklists->count());
    }

}
