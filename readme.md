# Checklist

Implementation of https://kw-checklist.docs.stoplight.io

Prerequisites:

1. PHP CLI >= 7.2 + extensions (common, mbstring, sqlite3)
2. Composer >= 1.8.x
3. Sqlite3 >= 3.22.x

Setup:

1. Install dependencies `composer install`
2. Initialize database (`sqlite3`) `php artisan migrate:fresh --seed`
3. Test `vendor/bin/phpunit`
4. Run `php -S localhost:8000 -t public`

Example authenticated API call (using `api_token`):

```
curl -i -X GET 'http://localhost:8000/checklists?api_token=thisisanapitoken'
```


