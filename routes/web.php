<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'checklists', 'middleware' => 'auth'], function () use ($router) {
    // checklists
    $router->get('/', ['as' => 'checklists.index', 'uses' => 'ChecklistController@index']);
    $router->post('/', 'ChecklistController@store');
    $router->get('{id}', ['as' => 'checklists.show', 'uses' => 'ChecklistController@show']);
    $router->patch('{id}', 'ChecklistController@update');
    $router->delete('{id}', 'ChecklistController@destroy');

    // items
    $router->post('/complete', 'ItemController@complete');
    $router->post('/incomplete', 'ItemController@incomplete');
    $router->get('{id}/items', ['as' => 'checklists.showItems', 'uses' => 'ChecklistController@showItems']);
    $router->post('{id}/items', 'ItemController@store');
    $router->get('{id}/items/{itemId}', ['as' => 'checklists.showItem', 'uses' => 'ItemController@show']);
    $router->patch('{id}/items/{itemId}', 'ItemController@update');
    $router->delete('{id}/items/{itemId}', 'ItemController@destroy');
    $router->post('{id}/items/_bulk', 'ItemController@bulkUpdate');
    $router->get('/items/summaries', 'ItemController@summaries');
});

$router->group(['prefix' => 'templates', 'middleware' => 'auth'], function () use ($router) {
    $router->get('/', ['as' => 'templates.index', 'uses' => 'TemplateController@index']);
    $router->post('/', 'TemplateController@store');
    $router->get('{id}', ['as' => 'templates.show', 'uses' => 'TemplateController@show']);
    $router->patch('{id}', 'TemplateController@update');
    $router->delete('{id}', 'TemplateController@destroy');
    $router->post('/{id}/assigns', 'TemplateController@assigns');
});

$router->group(['prefix' => 'histories', 'middleware' => 'auth'], function () use ($router) {
    $router->get('/', ['as' => 'histories.index', 'uses' => 'HistoryController@index']);
    $router->get('{id}', ['as' => 'histories.show', 'uses' => 'HistoryController@show']);
});